import React, { Component } from 'react';
import './insta.css';
import { FaCamera } from "react-icons/fa";
import {FiSend} from "react-icons/fi"

export default class Instaa extends Component{

    render(){
        return(
            <div>
            <div class="container">
            <div class="row justify-content-md-center mt-4">
            <div class="col col-lg-2">
                <FaCamera className="instaicon ml-5"/>
            </div>
            <div class="col-md-auto">
                <p className="text-center mr-5">Instagram</p>
             </div>
             <div class="col col-lg-2">
              <FiSend className="instaicon mr-2"/>
            </div>
            </div>
            </div>
            </div>
        )
    }
}