import React, { Component } from 'react';
import Login from './login';
import './insta.css';
import { FaUser, FaGooglePlusSquare, FaTwitterSquare } from "react-icons/fa";
import { IoMdKey } from "react-icons/io";
import { AiFillFacebook } from "react-icons/ai";
export default class Insta extends Component{
    render(){
        return(
        <div className="img">
            <div className="container-fluid">
              <img src={require('./Image/logpg.jpeg')} className="img-fluid img"/>
              <div className="carousel-caption ">
              <div class="login-form">
              
             <form className="blu" >
             
                <h2 className="text-white float">Sign in</h2>  
                <h2 className="icon"><AiFillFacebook className="icon icon-wi"/><FaGooglePlusSquare className="icon icon-wi"/>
                     <FaTwitterSquare className="icon icon-wi"/></h2> 
                <div className="form-group">
        	  <div className="input-group">
                <span className="input-group-addon input"><FaUser className="fa" /></span>
                <input type="text" className="form-control" name="username" placeholder="Username" required="required"/>				
            </div>
           </div>
		    <div className="form-group">
            <div className="input-group">
                <span className="input-group-addon input"><IoMdKey className="fa"/></span>
                <input type="password" className="form-control" name="password" placeholder="Password" required="required"/>				
            </div>
        </div>        
        <div className="form-group">
            <button type="submit" className="btn btn-warning btn-block">Sign in</button>
        </div>
        <div className="clearfix">
            <label className="float checkbox-inline"><input type="checkbox"/> Remember me</label>
        </div>
        <div className="">
          <p className="text-center mt-5 text-white small">Don't have an account? <a href="#">Sign up here!</a></p>
        <p className="small"> <a href="#">Forgot Your Password!</a></p>
        </div>

		</form>
    
       </div>
               </div>   
              </div>
             </div>
            
       
        )
    }
}